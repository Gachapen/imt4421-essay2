\section*{Introduction}
Research has been published through scholarly journals for several hundred years after the first journals appeared in 1665~\cite{OpenAccess}.
Most of the literature has been and is still difficult to access.
There are two main barriers hindering access to the literature~\cite{OpenAccess}.
The first barrier is the simplest and most obvious: the price tag.
Getting access to scientific literature can cost more than one can afford, especially if e.g.\ one researcher needs to access several tens or hundreds of articles.
While a reader can access and read an article when they have overcome the price tag barrier by paying for access, there is still the barrier of copyright.
The author, or more commonly the publisher, owns the copyright for the work, which means that a reader can't e.g.\ copy, reformat or translate it without the consent of the copyright owner~\cite{OpenAccess}.

These two barriers are what open access (OA) seeks to tear down.
As Peter Suber writes in his book~\cite{OpenAccess}:~\say{Open access (OA) literature is digital, online, free of charge, and free of most copyright and licensing restrictions}.
By being free of charge, the price tag barrier is gone, and by being free of most copyright restrictions, the copyright barrier is diminished.

\section*{Effect of Open Access}
Open access appears as a model that will solve \say{[the] crisis of accessibility to the scientific literature}~\cite{Impact}.
Results from studies where surveys and interviews have been used show that researchers generally are happy with their access to scientific literature~\cite{Impact}.
This suggests that there might not exist an access crisis to be solved at all.
Various studies look at how big impact OA has on research and practical use.
One of the main approaches to measuring the impact of OA is bibliometric by looking at the amount of cites and downloads an article has received.

Some earlier studies report that OA to scientific literature has a large effect on citations, but McCabe and Snyder argue that the effects \say{are simply artifacts of the researchers' inability to control for important covariates such as time and differences in article quality}~\cite{Impact}, meaning that the results might not be valid.
Later research support this argument by doing more controlled experiments that show that OA actually only have modest or no impact at all on the number of cites~\cite{Impact,Random}.

While studies show that there is no effect on cites, they do show that OA has a large effect on the number of downloads of articles~\cite{Impact}.
An experiment shows that the number of full-text (HTML) downloads doubled when providing articles as OA, while the download of full image (PDF) increased by 62\%~\cite{Impact}.
The study concludes that the increase in download numbers, but not in citations, suggests that the readership only increases outside the core author community.

\section*{Challenges and Possibilities}
Many challenges have already been overcome during the years~\cite{OpenAccess}, but some still persist.
One of the biggest challenges to get a broader use, is the publication fees~\cite{OpenAccess}.
Studies show that while authors generally think of OA as a good principle, \say{66 per cent of authors would prefer publishing in a journal that is not an open access author-pays journal}~\cite{Attracted}.
This means that the price tag barrier has not been removed, but rather moved from the reader side to the author side.
In a world of only OA, scientific literature will be more accessible to researchers at low-budget institutions, but publishing their own scientific literature will be harder.

Results from some studies might suggest that OA journals need increased quality, as authors \say{care little about access status and copyright issues} and more \say{about the journal's reputation, readership, impact factor and speed of publication}~\cite{Impact}.
Also better information on what OA journals there are in specific fields would help, as authors state that they are unfamiliar with, and have issues finding suitable OA journals in their field~\cite{Attracted}.

The general idea seems to be that OA will make required research knowledge accessible in developing countries, though the results from experiments show minimal difference in number of citations.
There is a much larger difference in the number of downloads, which suggests that OA publications might mostly benefit consumers rather than contributors~\cite{Impact}.

Overall, not enough knowledge about OA and OA journals, the fee for publishing in an OA journal and the perceived knowledge about OA journal quality are the biggest challenges. The largest possibility of OA is to bring knowledge to not only researchers, but also consumers.
